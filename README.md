# About
This Programm finds complex Primes in the field of (-N,-N) to (N,N)

# How it works
A complex number a+bi is prime if and only if it satisfies one of the following properties
- If $a == 0$ then if $|b|$ is a real Prime and $|b|%4 == 3$
- If $b == 0$ then if $|a|$ is a real Prime and $|a|%4 == 3$
- Else if $a^2+b^2$ is a real Prime

since $a$ and $b$ are interchangeable and $|a|,a^2>0$ in the check, the search can be optimized from

```python
for i in range(N):
    for j in range(N):
        if is_comp_prime(complex(i,j))
            prime_set.add(complex(i,j))
``` 
to 
```python
for i in range(N):
    for j in range(i):
        if is_comp_prime(complex(i,j))
            prime_set.add(complex(i,j))
            prime_set.add(complex(j,i))
``` 
and since Primes greater than 2 are always odd
```python
for i in range(1, N):
    for j in range(1+i%2, i, 2):
        if is_comp_prime(complex(i,j))
            prime_set.add(complex(i,j))
            prime_set.add(complex(j,i))
``` 
![Complex Primes](Examples/complex_primes_500.png "Complex Primes to 500+500i")
