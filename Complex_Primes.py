from matplotlib import pyplot as plt
import numpy as np
import time

N = 500
max_ = 2*N**2
new_Prime = 0
print(max_)
with open("Input/prime_numbers.txt", "r") as FILE:
    R_P = set()
    R_P.add(2)
    line = FILE.readline()
    while line != "" and new_Prime <= max_:
        new_Prime = int(line)
        R_P.add(new_Prime)
        line = FILE.readline()

print("read")

def is_prime(x:int) -> bool:
    x = int(x)
    if x in R_P:
        return True
    else:
        return False

tic = time.perf_counter()

prime_set = set()
prime_set.add(complex(1,1))

for i in range(1,N):
    i_s = i**2
    for j in range(1+i%2, i, 2):
        if is_prime(i_s+j**2):
            prime_set.add(complex(i,j))
for i in range(3,N,4):
    if is_prime(i):
        prime_set.add(complex(i,0))

primes = np.array(list(prime_set))

toc = time.perf_counter()
print(toc-tic)

n = len(primes)
'''
diff = np.ones((n-1,n), dtype=int)

def closest_node(i, diff, primes):
    num = primes[i]
    diff[i,:] = np.abs(primes[:]-num)
    if num.imag != 0:
        diff[i,i] = min([abs(num.real-num.imag), 2*num.imag])
    else: 
        diff[i,i] = diff[i,i-1]

for i in range(n-1):
    closest_node(i, diff, primes)

differences = np.min(diff, axis=0) '''

diff = np.ones((n), dtype=int)
differences = np.ones((n), dtype=int)
#'''
num = primes[0]
differences[:] = np.abs(primes[:]-num)
if num.imag != 0:
    differences[0] = min([abs(num.real-num.imag), 2*num.imag])
else: 
    differences[0] = differences[1]

for i in range(1,n-1):
    num = primes[i]
    diff = np.abs(primes-num)
    if num.imag != 0:
        diff[i] = min([abs(num.real-num.imag), 2*num.imag])
    else: 
        diff[i] = diff[i-1]
    differences = np.min([differences,diff], axis=0)#'''

plane_array = np.zeros((2*N,2*N), dtype=int)

for i in range(n):
    a = int(primes[i].real)
    b = int(primes[i].imag)
    c = differences[i]
    for [re, im] in [[a,b],[a,-b], [-a,b], [-a,-b], [b,a],[b,-a], [-b,a], [-b,-a]]:
        plane_array[N+re,N+im] = c

plane_array[N+1,N+1] = 1
plane_array[N-1,N+1] = 1
plane_array[N+1,N-1] = 1
plane_array[N-1,N-1] = 1

print(time.perf_counter()-toc)

fig1 = plt.figure(figsize=(11, 11))  # instantiate a figure to draw
ax1 = plt.axes()  # create an axes object
ax1.set_xlim(0,2*N)
ax1.set_ylim(0,2*N)
ax1.axis("tight")
ax1.axis("off")

import matplotlib as mpl
import matplotlib.cm as cm
cmap = cm.get_cmap("gist_rainbow").copy()

#cmap.set_bad("white",1)
#cmap.set_under("white",1)

upper = cmap(np.arange(150))

lower = np.array([1,1,1,1])
cmap = np.vstack((lower, upper))
cmap = mpl.colors.ListedColormap(cmap,name="my",N=cmap.shape[0])

plt.imsave("Output_Archive/Number_Things/complex_primes_" + str(N) + ".png", plane_array, cmap = cmap)
plt.pcolormesh(plane_array, shading = 'flat', cmap=cmap)

plt.show()